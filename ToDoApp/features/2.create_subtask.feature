Feature: Create Subtask
          As a ToDo App user
          I should be able to create a subtask
          So I can break down my tasks in smaller pieces

  @create_subtask
    Scenario: Create subtask
      Given that I'm on the tasks screen
      And I click on the button My Tasks
      And I'll see list the tasks
      When I click on the Manage Subtasks the My first task
      And I see the title Create a SubTask
      And I fill the Subtask Description with My first subtask
      And I fill the Due Date and click on the button +Add
      And I see my subtask in the list and to close the window
	  And I click on the Manage Subtasks the My second task
      And I see the title Create a SubTask
      And I fill the Subtask Description with My second subtask
      And I fill the Due Date and press enter
      And I see my subtask in the list and to close the window
      Then I see my tasks with yours subtasks in the list
      And I sign out on the ToDo App

  @remove_task_and_subtask
    Scenario: Remove tasks and subtasks
      Given that I'm on the tasks screen list
      And I click the button My Tasks
      And I see list the tasks
      When I click the Manage Subtasks the My first task
      And I see the title Create SubTask
      And I click on the button remove substask
	  And I click close the window subtask
	  And I click remove second task
	  And I click on the button Remove the task with your subtask
      Then I'll see list the task empty
      And I sign out on the ToDo App system
