class DefaultPages
	include Capybara::DSL
	include RSpec::Matchers
	

	def home
		visit DEFAULT_DATA["url"]["home"]
	end

	def link_sign_in
		click_link 'Sign In'
	end

	def screen_sign_in		
		expect(page).to have_content("Sign in")		
	end

	def fill_mail
		page.fill_in("user[email]", :with => DEFAULT_DATA["todoappusr"]["mail"])
	end
	
	def fill_password
		page.fill_in('user[password]', :with => DEFAULT_DATA["todoappusr"]["pass_1"])
	end

	def button_sign_in		
		click_button "Sign in"
	end

	def button_remove		
		click_button "Remove"
	end

	def link_sign_out
		click_link "Sign out"
	end	

	def screen_sign_in_logged		
		expect(page).to have_content("Signed in successfully.")		
	end

	def screen_my_tasks		
		expect(page).to have_content("My Tasks")		
	end

	def link_my_tasks
		 find(:xpath, "/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a").click
	end

	def button_my_tasks
		page.find("a.btn.btn-lg.btn-success").click
		# find(:xpath, "/html/body/div[1]/div[2]/center/a").click
	end	

	def to_do_list_my_tasks		
		expect(page).to have_content("Jessé Dantas Lima's ToDo List")		
	end

	def fill_new_task_click		
		find(:id, "new_task").click
	end

	def fill_new_task
		page.fill_in("new_task", :with => DEFAULT_DATA["data"]["task_1"])
	end

	def button_add_task_click
		find(:xpath, "/html/body/div[1]/div[2]/div[1]/form/div[2]/span").click
	end

	def fill_second_task
		page.fill_in("new_task", :with => DEFAULT_DATA["data"]["task_2"])
	end

	def add_task_press_enter
		find(:xpath, '//*[@id="new_task"]').send_keys(:enter)
	end

	def my_first_task_recorded		
		page.has_content?("first task")		
	end

	def my_second_task_recorded		
		page.has_content?("second task")		
	end

	def button_add_first_task_click
		find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[2]/td[4]/button").click
	end

	def button_add_second_task_click
		find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[4]/button").click
	end

	def button_remove_task
		find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button").click
	end


# Subtasks

	def title_subtask
		page.has_content?("Create a SubTask")
	end

	def fill_new_subtask
		page.fill_in("new_sub_task", :with => DEFAULT_DATA["data"]["subtask_1"])
	end

	def add_subtask_press_enter
		find(:xpath, '//*[@id="new_sub_task"]').send_keys(:enter)
	end

	def duedate_press_enter
		find(:xpath, '//*[@id="dueDate"]').send_keys(:enter)
	end

	def fill_second_subtask
		page.fill_in("new_sub_task", :with => DEFAULT_DATA["data"]["subtask_2"])
	end

	def button_add_subtask_click		
		click_button "Add"
	end

	def my_first_subtask_recorded		
		page.has_content?("first subtask")
	end

	def my_second_subtask_recorded		
		page.has_content?("second subtask")
	end

	def fill_due_date_1
		page.fill_in("due_date", :with => DEFAULT_DATA["data"]["due_date_1"])
	end

	def fill_due_date_2
		page.fill_in("due_date", :with => DEFAULT_DATA["data"]["due_date_2"])
	end

	def button_close_subtask		
		click_button "Close"
	end

	def button_remove_subtask		
		click_button "Remove SubTask"
	end

end
