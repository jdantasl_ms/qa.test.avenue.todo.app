Given(/^that I'm on the tasks screen list$/) do
  DefaultPages.new.home
  DefaultPages.new.link_sign_in
  DefaultPages.new.screen_sign_in
  DefaultPages.new.fill_mail
  DefaultPages.new.fill_password
  DefaultPages.new.button_sign_in
  DefaultPages.new.screen_my_tasks
end

Given(/^I click the button My Tasks$/) do
  DefaultPages.new.button_my_tasks
end

Given(/^I see list the tasks$/) do
  DefaultPages.new.to_do_list_my_tasks
end

When(/^I click the Manage Subtasks the My first task$/) do
  DefaultPages.new.button_add_first_task_click
end

When(/^I see the title Create SubTask$/) do
  DefaultPages.new.title_subtask
end

When(/^I click on the button remove substask$/) do
  DefaultPages.new.button_remove_subtask
end

When(/^I click close the window subtask$/) do
  DefaultPages.new.button_close_subtask
end

When(/^I click remove second task$/) do
  DefaultPages.new.button_remove_task
end

When(/^I click on the button Remove the task with your subtask$/) do
  DefaultPages.new.button_remove
end

Then(/^I'll see list the task empty$/) do
  DefaultPages.new.link_my_tasks
end

Then(/^I sign out on the ToDo App system$/) do
  DefaultPages.new.link_sign_out
end
