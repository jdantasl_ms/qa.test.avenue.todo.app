Given(/^that I'm on the tasks screen$/) do
  DefaultPages.new.home
  DefaultPages.new.link_sign_in
  DefaultPages.new.screen_sign_in
  DefaultPages.new.fill_mail
  DefaultPages.new.fill_password
  DefaultPages.new.button_sign_in
  DefaultPages.new.screen_my_tasks
end

Given(/^I click on the button My Tasks$/) do
  DefaultPages.new.button_my_tasks
end

Given(/^I'll see list the tasks$/) do
  DefaultPages.new.to_do_list_my_tasks
end

When(/^I click on the Manage Subtasks the My first task$/) do
  DefaultPages.new.button_add_first_task_click
end

When(/^I see the title Create a SubTask$/) do
  DefaultPages.new.fill_new_subtask
end

When(/^I fill the Subtask Description with My first subtask$/) do
  DefaultPages.new.fill_new_subtask
end

When(/^I fill the Due Date and click on the button \+Add$/) do
  DefaultPages.new.fill_due_date_1
  DefaultPages.new.button_add_subtask_click
end

When(/^I see my subtask in the list and to close the window$/) do
  DefaultPages.new.button_close_subtask
end

When(/^I click on the Manage Subtasks the My second task$/) do
  DefaultPages.new.my_second_subtask_recorded
  DefaultPages.new.button_add_second_task_click
end

When(/^I fill the Subtask Description with My second subtask$/) do
  DefaultPages.new.fill_second_subtask
end

When(/^I fill the Due Date and press enter$/) do
  DefaultPages.new.fill_due_date_2
  DefaultPages.new.duedate_press_enter
end

Then(/^I see my tasks with yours subtasks in the list$/) do
  DefaultPages.new.my_first_task_recorded
  DefaultPages.new.my_second_task_recorded
end

Then(/^I sign out on the ToDo App$/) do
  DefaultPages.new.link_sign_out
end
