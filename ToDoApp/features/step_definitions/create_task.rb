Given(/^that I'm on the My Tasks screen$/) do
  DefaultPages.new.home
  DefaultPages.new.link_sign_in
  DefaultPages.new.screen_sign_in
  DefaultPages.new.fill_mail
  DefaultPages.new.fill_password
  DefaultPages.new.button_sign_in
  DefaultPages.new.screen_my_tasks
end

Given(/^I click on the link My Tasks, next link the Home$/) do
  DefaultPages.new.link_my_tasks
end

Given(/^I'll see list my tasks$/) do
  DefaultPages.new.to_do_list_my_tasks
end

When(/^I fill the a first task$/) do
  sleep(3)
  DefaultPages.new.fill_new_task  
end

When(/^I click on the button \(\+\)$/) do  
  sleep(3)
  DefaultPages.new.button_add_task_click
end

When(/^I click on the field new task$/) do  
  sleep(3)
  DefaultPages.new.fill_new_task_click
end

When(/^I fill the a second task and press enter$/) do
  sleep(3)
  DefaultPages.new.fill_second_task
  DefaultPages.new.add_task_press_enter
  sleep(5)
end

Then(/^I see my tasks in the list$/) do
  DefaultPages.new.my_first_task_recorded
  DefaultPages.new.my_second_task_recorded
end

Then(/^I sign out on the system$/) do
  DefaultPages.new.link_sign_out
end
