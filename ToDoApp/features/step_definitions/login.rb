Given(/^that I visit the home ToDo App$/) do
  DefaultPages.new.home
end

When(/^I to access the link Sign In$/) do
  DefaultPages.new.link_sign_in
end

When(/^the screen login be loaded$/) do
  DefaultPages.new.screen_sign_in
end

When(/^I fill the mail$/) do
  DefaultPages.new.fill_mail
end

When(/^I fill the password$/) do
  DefaultPages.new.fill_password
end

When(/^I click button Sign In$/) do
  DefaultPages.new.button_sign_in
end

Then(/^I'll be logged$/) do
  DefaultPages.new.screen_sign_in_logged
end