Feature: Create Task
          As a ToDo App user
          I should be able to create a task
          So I can manage my tasks

  @login
    Scenario: Login on ToDo App
      Given that I visit the home ToDo App
      When I to access the link Sign In
      And the screen login be loaded
      And I fill the mail
      And I fill the password
      And I click button Sign In
      Then I'll be logged

  @create_task
    Scenario: Create task
      Given that I'm on the My Tasks screen
      And I click on the link My Tasks, next link the Home
      And I'll see list my tasks
      When I fill the a first task
      And I click on the button (+)
      And I click on the field new task
      And I fill the a second task and press enter
      Then I see my tasks in the list      
      And I sign out on the system